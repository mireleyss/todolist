<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/main.css') }}">
    <title>{{ $task->title }}</title>
</head>
<body>

    <div class="show-data">
        <h1>{{ $task->title }}</h1>
        <h2>Description: </h2>
        <p>{{ $task->description }}</p>
        <h2>Due: </h2>
        <p>{{ $task->deadline }}</p>

        <a href="{{ route('tasks.edit', $task) }}">Edit</a>
        <a href="{{ route('tasks.index') }}">Back</a>
    </div>

</body>
</html>
