<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/main.css') }}">
    <title>{{ $task->title }}: edit</title>
</head>
<body>

    <h1>Add task</h1>

    <form action="{{ route('tasks.update', $task) }}" method="POST" class="data-input">
        @csrf
        @method('PUT')

        <div>
            <label>Title</label>
            <input type="text" name="title" value="{{ $task->title }}">
        </div>

        <div>
            <label>Description</label>
            <textarea name="description">{{ $task->description }}</textarea>
        </div>
        <div>
            <label>Deadline</label>
            <input type="datetime-local" name="deadline" value="{{ $task->deadline }}">
        </div>

        <button type="submit" class="save-btn">Save</button>
    </form>
    <a href="{{ route('tasks.index') }}">Cancel</a>

</body>
</html>
