<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/main.css') }}">
    <title>Add task</title>
</head>
<body>

    <h1>Add task</h1>

    <form action="{{ route('tasks.store') }}" method="POST" class="data-input">
        @csrf

        <div>
            <label>Title</label>
            <input type="text" name="title" placeholder="Title">
        </div>

        <div>
            <label>Description</label>
            <textarea name="description" placeholder="Description"></textarea>
        </div>
        <div>
            <label>Deadline</label>
            <input type="datetime-local" name="deadline">
        </div>

        <button type="submit" class="save-btn">Save</button>
    </form>
    <a href="{{ route('tasks.index') }}">Cancel</a>

</body>
</html>
